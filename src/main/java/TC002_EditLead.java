import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

public class TC002_EditLead extends Annotations{
	@BeforeClass(groups="sanity")
	public void setData() {
		String testcaseName = "TC002_EditLead";
		String testcaseDec = "Edit the Lead in leaftaps";
		String author = "Gayatri";
		String category = "Smoke";
	}
	@Test(groups="sanity", dependsOnGroups="smoke")
	public void editLead() throws InterruptedException {
		click(locateElement("link", "CRM/SFA"));
		click(locateElement("link", "Leads"));
		click(locateElement("link", "Find Leads"));
		click(locateElement("xpath","//span[text()='Phone']"));
		clearAndType(locateElement("name", "phoneNumber"), "99"); 
	    click(locateElement("xpath","//button[text()='Find Leads']"));
	 /*   Thread.sleep(1000);
	    click(locateElement("xpath", "//div[@class='x-grid3-cell-inner x-grid3-col-partyId']/a"));
	    click(locateElement("link", "Edit"));
	    clearAndType(locateElement("id","updateLeadForm_companyName"), "TCS");
	    click(locateElement("name", "submitButton"));*/


	}

}

