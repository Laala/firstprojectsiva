package com.yalla.selenium.api.base;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class Reports {
	
	public void startReport() {
		ExtentHtmlReporter reporter= new ExtentHtmlReporter("./reports/result.html");
		ExtentReports extent=new ExtentReports();
		extent.attachReporter(reporter);
		ExtentTest test=extent.createTest("MergeLead","Merging two leads");
		
	}

}
