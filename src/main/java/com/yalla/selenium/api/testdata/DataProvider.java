package com.yalla.selenium.api.testdata;

import java.io.IOException;

import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class DataProvider {

	public static void main(String[] args) throws IOException {
		// TODO Auto-generated method stub
XSSFWorkbook wbook=new XSSFWorkbook("./data/createLead.xlsx");
XSSFSheet sheet = wbook.getSheetAt(0);
int rowcount = sheet.getLastRowNum();
System.out.println("Row count is " +rowcount);
short colcount = sheet.getRow(0).getLastCellNum();
System.out.println("Column count is " +colcount);
for (int i = 1; i <=rowcount; i++) {
	XSSFRow row = sheet.getRow(i);
	for (int j = 0; j < colcount; j++) {
		XSSFCell cell = row.getCell(j);
		System.out.println(cell.getStringCellValue());
		
	} 
}
	}

}
