package com.yalla.selenium.api.design;

import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.WebElement;

public interface NewFramework {

	public void startApp(String browser, String url);
	/**
	 * This method will locate the element using any given locator
	 * @param locatorType  - The locator by which the element to be found
	 * @param locValue - The locator value by which the element to be found
	 * @author Siva - TestLeaf
	 * @throws NoSuchElementException
	 * @return The first matching element on the current context.
	 */
public WebElement locateElement(String locatorType, String value);	
	
	/**
	 * This method will locate the element using id
	 * @param locValue - The locator value by which the element to be found
	 * @author Siva - TestLeaf
	 * @throws NoSuchElementException
	 * @return The first matching element on the current context.
	 */
public void click(WebElement ele);


/**
 * This method will enter the value in the given text field 
 * @param ele   - The Webelement (text field) in which the data to be entered
 * @param data  - The data to be sent to the webelement
 * @see locateElement method in Browser Class
 * @author Sarath - TestLeaf
 * @throws ElementNotInteractable,IllegalArgumentException(throws if keysToSend is null)	
 */
public void clearAndType(WebElement ele,String data);

/**
 * This method will get the visible text of the element
 * @param ele   - The Webelement (button/link/element) in which text to be retrieved
 * @author Sarath - TestLeaf
 * @see locateElement method in Browser Class
 */
public void takeSnap();

/**
 * This method will close the active browser
 * @author Siva - TestLeaf
 */
public void close();

/**
 * This method will close all the browsers
 * @author Siva - TestLeaf
 */
}
