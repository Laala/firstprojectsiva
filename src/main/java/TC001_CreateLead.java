	
	import org.testng.annotations.BeforeClass;
import org.testng.annotations.DataProvider;
import org.testng.annotations.Test;

import com.yalla.selenium.api.base.Annotations;

	public class TC001_CreateLead extends Annotations{
		@BeforeClass
		public void setData() {
			String testcaseName = "TC001_CreateLead";
			String testcaseDec = "Create a new Lead in leaftaps";
			String author = "Gayatri";
			String category = "Smoke";
		}
		@Test(dataProvider="createData")
		public void createLead(String compName, String FstName, String LastName) {
			click(locateElement("link", "CRM/SFA"));
			click(locateElement("link", "Leads"));
			click(locateElement("link", "Create Lead"));
			clearAndType(locateElement("id", "createLeadForm_companyName"), compName);
			clearAndType(locateElement("id", "createLeadForm_firstName"), FstName);
			clearAndType(locateElement("id", "createLeadForm_lastName"),LastName);
			click(locateElement("name", "submitButton")); 
		}

	@DataProvider(name="createData")
		public Object[][] fetchData(){
		Object[][] data=new Object[2][3];
		data[0][0]="CTS";
		data[0][1]="Siva";
		data[0][2]="Vijay";
		
		data[1][0]="Wipro";
		data[1][1]="Cva";
		data[1][2]="Vj";
		return data;
		
	}
	
	}



